# This file is part of Active Archives.
# Copyright 2006-2011 the Active Archives contributors (see AUTHORS)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Also add information on how to contact you by electronic and paper mail.


"""
Active Archives aacore views
"""


import RDF
from urlparse import urlparse

from django.http import (HttpResponse, HttpResponseRedirect)
from django.shortcuts import (render_to_response, get_object_or_404)
from django.template import RequestContext 

from aacore import RDF_MODEL
import rdfutils
from aacore.models import (Namespace, Resource)
from aacore.utils import (add_resource, is_local_url, full_site_url)


def rdf_delegate(request, id):
    """
    RDF Delegate view
    """
    context = {}
    # FIXME: Resolve RDFSource
    source = get_object_or_404(RDFSource, pk=id)
    context['source'] = source
    return render_to_response("aacore/rdf_source.html", context, context_instance=RequestContext(request))


def namespaces_css (request):
    """
    Generates a stylesheet with the namespace colors.

    **Context**

    ``RequestContext``

    ``namespaces``
        A queryset of all :model:`aacore.Namespace`.

    **Template:**

    :template:`aacore/namespaces.css`
    """
    context = {}
    context['namespaces'] = Namespace.objects.all()
    return render_to_response("aacore/namespaces.css", context, 
                              context_instance=RequestContext(request), mimetype="text/css")


def resources (request):
    """
    Dumps a list of all the :model:`aacore.Resource` objects.

    **Context**

    ``RequestContext``

    ``resources``
        A queryset of all :model:`aacore.Resource`.

    **Template:**

    :template:`aacore/resources.html`
    """
    context = {}
    context['resources'] = Resource.objects.all()
    return render_to_response("aacore/resources.html", context, 
                              context_instance=RequestContext(request))


def browse (request):
    """
    Provides an interface to browse the RDF metadata of a :model:`aacore.Resource`. 
    Creates the :model:`aacore.Resource` if the given URI isn't indexed yet.

    **Context**

    ``RequestContext``

    ``resource``
        An instance of :model:`aacore.Resource`.

    ``literal``
        the current browsed litteral if any. Is loaded with an empty string if
        the browsed expression is a uri.

    ``node``
        the current browsed URI or literal if any.

    ``namespaces``
        A queryset of all :model:`aacore.Namespace`.
    
    ``links_as_rel`` 
        To be documentated

    ``links_in``
        To be documentated
        
    ``links_out``
        To be documentated

    ``node_stats``
        To be documentated

    **Template:**

    :template:`aacore/browse.html`
    """
    node = request.REQUEST.get("node")
    do_reload = request.REQUEST.get("_submit") == "reload"

    # If nothing is browsed, simply return an empty form
    if not node:
        return render_to_response("aacore/browse.html", {}, 
                                  context_instance=RequestContext(request))

    # RDF distinguishes URI and literals...
    is_literal = urlparse(node).scheme not in ('file', 'http', 'https')

    # Avoids browsing an internal URL
    if is_local_url(node):
        return HttpResponseRedirect(node)

    # force every (http) resource to be added
    # TODO: REQUIRE LOGIN TO ACTUALLY ADD...
    if do_reload or not is_literal:
        add_resource(node, request=request, reload=do_reload)

    context = dict(namespace=Namespace.objects.all(), node=node, literal=is_literal)

    context.update(rdfutils.load_links(RDF_MODEL, node, literal=is_literal))

    if not is_literal:
        try:
            resource = Resource.objects.get(url=node)
            context['resource'] = resource
        except Resource.DoesNotExist:
            pass

    return render_to_response("aacore/browse.html", context, 
                              context_instance=RequestContext(request))


def resource_sniff (request, id):
    """
    This is the generic sniff view for Resources,
    it provides the basic resource data (from HTTP, etc) data as RDFa for indexing
    """
    context = {}
    context['namespaces'] = Namespace.objects.all()
    context['resource'] = get_object_or_404(Resource, pk=id)
    return render_to_response("aacore/resource_sniff.html", context, 
                              context_instance=RequestContext(request))


FORMAT_MIMETYPES = {
    "rdfxml":       "application/rdf+xml",
    "json":         "application/json",
    "json-triples": "application/json"
}


def reindex (request):
    url = request.REQUEST.get("url")
    url = full_site_url(url)
    format = request.REQUEST.get("format")
    rdfutils.rdf_parse_into_model(RDF_MODEL, url, format)
    return HttpResponseRedirect(url)
    context['node_stats'] = node_stats
    context['links_out'] = links_out
    context['links_in'] = links_in
    context['links_as_rel'] = as_rel


def dump (request):
    """
    url:    input url (optional, if not given displays the contents of the store)
    input:  format of input url if given
    format: output format (default ntriples), possible values include: ntriples, rdfxml, turtle, dot, json, json-triples
    """
    url = request.REQUEST.get("url", "")
    if not url:
        model = RDF_MODEL
    else:
        inputformat = request.REQUEST.get("input", "")
        # parser = RDF.Parser(name="rdfa")
        if inputformat:
            parser = RDF.Parser(name=inputformat)
        else:
            parser = RDF.Parser()
        model = RDF.Model()
        parser.parse_into_model(model, url, base_uri=url)

    format = str(request.REQUEST.get("output", "turtle"))
    ser = RDF.Serializer(name=format)

    for n in Namespace.objects.all():
        ser.set_namespace(n.name.encode("utf-8"), RDF.Uri(n.url.encode("utf-8") ))

    # dc: http://purl.org/dc/elements/1.1/
    # xmls: http://www.w3.org/2001/XMLSchema#

    mime = FORMAT_MIMETYPES.get(format, "text/plain")
    return HttpResponse(ser.serialize_model_to_string(model), mimetype=mime + ";charset=utf-8")


def query (request):
    context = {}
    q = request.REQUEST.get("query", "")
    context['query'] = q
    context['namespaces'] = Namespace.objects.all()
    if q and request.method == "POST":
        thequery = RDF.Query(q.encode("utf-8"), query_language="sparql")
        results = thequery.execute(RDF_MODEL)
        context['results'] = results.get_bindings_count()
        bindings = []
        for i in range(results.get_bindings_count()):
            bindings.append(results.get_binding_name(i))
        context['bindings'] = bindings

        rows = []
        for r in results:
            row = []
            for key in bindings:
                row.append(r.get(key))
            rows.append(row)
        context['rows'] = rows

    return render_to_response("aacore/query.html", context, 
                              context_instance=RequestContext(request))

